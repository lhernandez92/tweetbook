﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Tweetbook.Contracts.V1;
using Tweetbook.Controllers.V1.Requests;
using Tweetbook.Domain;

namespace Tweetbook.Tests
{
    [TestFixture]
    public class PostControllerTest : IntegrationTest
    {
        [SetUp]
        public void Setup()
        {
            
        }
        
        [Test]
        public async Task GetAll_WithoutAnyPosts_ReturnEmptyResponse()
        {
            //Act
            await AuthenticateAsync();
            var result = await _client.GetAsync(ApiRoutes.Posts.GetAll);

            //Assert
            Assert.IsTrue(result.StatusCode == HttpStatusCode.OK);
            Assert.AreEqual(result.Content.ReadAsAsync<List<Post>>().Result.ToList().Count, 0);
        }

        [Test]
        public async Task Get_ReturnPost_WhenPostExistInTheDatabase()
        {
            //Arrenge
            await AuthenticateAsync();
            var createdPost = await CreatePostAsync(new CreatePostRequest { Name = "Test post" });

            //Act
            var result = await _client.GetAsync(ApiRoutes.Posts.Get.Replace("{postId}", createdPost.Id.ToString()));
            var returnedPost = await result.Content.ReadAsAsync<Post>();

            //Assert
            Assert.IsTrue(result.StatusCode == HttpStatusCode.OK);
            Assert.AreEqual(returnedPost.Id, createdPost.Id);
            Assert.AreEqual(returnedPost.Name, "Test post");
        }

        [TearDown]
        public void TearDown()
        {
            Dispose();
        }
    }
}

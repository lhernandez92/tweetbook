using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Tweetbook.Contracts.V1;
using Tweetbook.Controllers.V1;
using Tweetbook.Controllers.V1.Requests;
using Tweetbook.Controllers.V1.Responses;
using Tweetbook.Data;

namespace Tweetbook.Tests
{
    public class IntegrationTest : IDisposable
    {
        protected readonly HttpClient _client;
        private readonly IServiceProvider _serviceProvider;

        public IntegrationTest()
        {
            var appFactory = ConfigureDBInMemory();
            _serviceProvider = appFactory.Services;
            _client = appFactory.CreateClient();            
        }

        protected async Task AuthenticateAsync()
        {
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", await GetJwtAsync());
        }

        protected async Task<PostResponse> CreatePostAsync(CreatePostRequest request)
        {
            var response = await _client.PostAsJsonAsync(ApiRoutes.Posts.Create, request);
            return await response.Content.ReadAsAsync<PostResponse>();
        }

        private async Task<string> GetJwtAsync()
        {
            var user = new RegistrationRequest { 
                Email = "webinar@quipu.com",
                Password = "TestPassword1234!"
            };

            var response = await _client.PostAsJsonAsync(ApiRoutes.Identity.Register, user);
            var registrationResponse = await response.Content.ReadAsAsync<AuthSuccessResponse>();

            return registrationResponse.Token;
        }

        private WebApplicationFactory<Startup> ConfigureDBInMemory()
        {
            var appFactory = new WebApplicationFactory<Startup>()
            .WithWebHostBuilder(builder => {
                builder.ConfigureServices(services => {
                    services.RemoveAll(typeof(DbContextOptions<DataDbContext>));
                    services.AddDbContext<DataDbContext>(options => {
                        options.UseInMemoryDatabase("TestDb");
                    });
                    services.BuildServiceProvider();
                });
            });            

            return appFactory;
        }

        public void Dispose()
        {
            var serviceScope = _serviceProvider.CreateScope();
            var context = serviceScope.ServiceProvider.GetService<DataDbContext>();
            context.Database.EnsureDeleted();
        }
    }
}
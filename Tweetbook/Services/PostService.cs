﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tweetbook.Data;
using Tweetbook.Domain;

namespace Tweetbook.Services
{
    public class PostService : IPostService
    {
        private readonly DataDbContext _dataDbContext;

        public PostService(DataDbContext dataDbContext)
        {
            _dataDbContext = dataDbContext;
        }

        public async Task<bool> CreatePostAsync(Post post)
        {
            await _dataDbContext.Posts.AddAsync(post);
            var result = await _dataDbContext.SaveChangesAsync();

            return result > 0;
        }

        public async Task<bool> DeletePostAsync(Guid postId)
        {
            var post = await GetPostByIdAsync(postId);

            if (post == null)
                return false;

            _dataDbContext.Posts.Remove(post);
            var result = await _dataDbContext.SaveChangesAsync();

            return result > 0;
        }

        public async Task<Post> GetPostByIdAsync(Guid postId)
        {
            return await _dataDbContext.Posts.SingleOrDefaultAsync(x => x.Id == postId);
        }

        public async Task<List<Post>> GetPostsAsync()
        {
            return await _dataDbContext.Posts.ToListAsync();
        }

        public async Task<bool> OwnPostsAsync(Guid postId, string userId)
        {
            var post = await _dataDbContext.Posts.AsNoTracking().SingleOrDefaultAsync(p => p.Id == postId);

            if (post == null || post.UserId != userId)
                return false;

            return true;
            
        }

        public async Task<bool> UpdatePostAsync(Post postToUpdate)
        {
            _dataDbContext.Posts.Update(postToUpdate);
            var result = await _dataDbContext.SaveChangesAsync();

            return result > 0;
        }
    }
}

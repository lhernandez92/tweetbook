﻿using System.ComponentModel.DataAnnotations;

namespace Tweetbook.Controllers.V1
{
    public class RegistrationRequest
    {
        [EmailAddress]
        public string Email { get; set; }

        public string Password { get; set; }
    }
}
﻿namespace Tweetbook.Controllers.V1
{
    public class LoginRequest
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}